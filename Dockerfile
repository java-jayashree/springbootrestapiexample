FROM java:8
RUN mkdir -p /java_f
WORKDIR /java_f
COPY target/SpringBootRestApiExample-1.0.0.jar /java_f/SpringBootRestApiExample-1.0.0.jar
CMD java -jar SpringBootRestApiExample-1.0.0.jar
