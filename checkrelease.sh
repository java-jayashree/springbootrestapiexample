#!/bin/sh

getrelease=$(helm ls myrestapi --tiller-namespace java | grep myrestapi | awk '{print $1}')

if [ $getrelease == myrestapi ]
then
(
helm delete --purge myrestapi --tiller-namespace java
)
fi
